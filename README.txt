###======================================================================#
#                                                                        #
# Copyright (C) 2015 Carlos Augusto de Souza Braga                       #
# <CASBraga@Gmail.com>                                                   #
#                                                                        #
#  This program is free software: you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation, either version 3 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#========================================================================#

This is a README file for the Python class Stopwatch which plays a game of 
Guess the number. Written in Python 2.7.10.

This class was made with the intent of being used in a windowing system 
that calls the event handler functions. At the moment, it is intrinsically 
conected to the simpleGUI package, used with http://www.codeskulptor.org/.
This is because I have yet to create a timer to generates ticks at
specified intervals (something already provided by simpleGUI). 

To test the class, copy the code to the editor found at http://www.codeskulptor.org/#.
Then press the run button and enjoy.

TODO:

Create a timer class to substitute the functionality of the simpleGUI package.
This is a priority since using any windowing system other than simpleGUI
will require the use of a timer. 

Or...

Redesign the class to no longer require a timer with the specific
functionality provided by simpleGUI.

Add GTK+ compatibility. This will take a while, since I am a veritable 
noob at GTK+. Also the timer comes first.
