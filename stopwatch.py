###======================================================================#
#                                                                        #
# Copyright (C) 2015 Carlos Augusto de Souza Braga                       #
# <CASBraga@Gmail.com>                                                   #
#                                                                        #
#  This program is free software: you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation, either version 3 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#========================================================================#

import random
import simplegui

class Stopwatch:
    """
    Class Stopwatch is a game in which the player must stop 
    a stopwatch at whole seconds, i. e. 1.0, 2.0, etc.
    
    Contains:
    
    Global variables:
        __count => integer containing the time in tenth of 
                 a second
        __score => integer containing the number of times
                 a player stops the watch at whole seconds
        __tries => integer containing the number of times the 
                 player stops the watch
        __time  => string containing time in the format
                 00:00:0 (min.:sec:1/10 of a sec)

    Methods:
        private methods (aka helper functions):
            __init__()
            __format(t)
            __game_logic()
        public methods (aka event handlers):
            start()
            stop()
            reset()
        getter methods (also public)
            get_score()
            get_tries()
            get_time()
    """

    # global variables initialization
    __count = None
    __score = None
    __tries = None
    __time  = None

    def __init__(self):
        """
        Constructor of the class, initializes 
        game state
        """
        
        # inititializes global variables
        self.__count = 0
        self.__score = 0
        self.__tries = 0
        self.__time  = "00:00.0"
    
    # define helper function format that converts time
    # in tenths of seconds into formatted string AB:CD.E
    def __format(self, t):
        """
        Converts time in deecimal of a second to the 
        format 0:00:0 (min.:seg.:dec.seg.) .
        
        t => integer containing the time to be converted
        """
 
        # local variables
        # t_minutes, u_minutes, t_seconds and u_seconds are strings corresponding
        # to t_minutes u_minutes : t_seconds u_seconds : str(dec_sec)
        t_minutes = ""
        u_minutes = ""
        t_seconds = ""
        u_seconds = ""
        dec_sec = 0
        
        # calculates minutes using integer division 
        # (min = t // 600); forces tens of minutes to be 
        # in the range 0-5
        t_minutes = str(((t // 600) // 10) % 60)
        
        # forces unit of minutes to be 
        # in the range 0-9
        u_minutes = str((t // 600) % 10)
        
        # calculates seconds using integer division 
        # and module (sec = t // 10); forces the tens 
        # of seconds to be in the range 0-5
        t_seconds = str(((t // 10) % 60) // 10)
        
        # forces unit of seconds to be in the range 0-9
        u_seconds = str((t // 10) % 10)
        
        # calculates decimal of seconds module 
        # (dec. sec. = t % 10)
        dec_sec = t % 10
        
        self.__time = t_minutes + u_minutes + ":" + t_seconds + u_seconds + ":" + str(dec_sec) 

    # helper function for game logic
    def __game_logic(self):
        """
        Determines if the stopwatch is stopped in whole seconds
        (0.0, 1.0, 2.0, ...)
        """

        # checks if count corresponds to whole seconds
        # using modulo arithmatic
        if (self.__count % 10 == 0 and self.__count != 0):
            self.__score += 1

    # define event handlers for buttons; "Start", "Stop", "Reset"
    def start(self):
        """
        Starts the timer
        """
        
        # starts timer
        timer.start()

    def stop(self):
        """
        Stops timer and add tries to score
        """
        
        # checks if the timer is running
        # updates score calling game_logic()
        # and tries it it is
        if (timer.is_running()):
            self.__tries += 1
            self.__game_logic()
            timer.stop()

    def reset(self):
        """
        Resets the timer and the score
        """

        # checks if the timer is running
        # stops timer
        if (timer.is_running()):
            timer.stop()
 
        # resets score, tries and timer
        self.__count = 0
        self.__score = 0
        self.__tries = 0
        self.__time  = "00:00:0"

    # define event handler for timer with 0.1 sec interval
    def watch(self):
        """
        Increases count by 1 every 0.1 second
        """

        self.__count += 1
        self.__format(self.__count)

    # define getter methods
    def get_score(self):
        """
        Get the current value of score
        """
        
        return self.__score
    
    def get_tries(self):
        """
        Get the current value of tries
        """
        
        return self.__tries
    
    def get_time(self):
        """
        Get the current value of time
        """
        
        return self.__time

# define draw handler
def draw(canvas):
    """
    Draws elements form the game to canvas
    
    canvas => object of class canvas
    """

    # global variables and objects
    global s_watch

    # local variables
    # list draw_str will hold the string to draw
    w_text   = 0
    draw_str = []
        
    # add the string to draw_str
    draw_str.insert(0, "score")
    draw_str.insert(1, str(s_watch.get_score()) + "/" + str(s_watch.get_tries()))
    draw_str.insert(2, s_watch.get_time())
            
    # draws str(score) and timer to canvas 
    # use of get_canvas_textwidth() to make sure 
    # text never leaves the canvas
    w_text = frame.get_canvas_textwidth(draw_str[0], 36)
    canvas.draw_text(draw_str[0], (300 - w_text - 10, 20), 36, "Green")
    w_text = frame.get_canvas_textwidth(draw_str[1], 36)
    canvas.draw_text(draw_str[1], (300 - w_text - 20, 50), 36, "Green")
    w_text = frame.get_canvas_textwidth(draw_str[2], 60)
    canvas.draw_text(draw_str[2], (300 - w_text - 50, 130), 60, "white")

# declare a Stopwatch object
s_watch = Stopwatch()

# create frame
frame = simplegui.create_frame("Test", 300, 200)

# register event handlers
b_start = frame.add_button("Start", s_watch.start, 200)
b_stop = frame.add_button("Stop", s_watch.stop, 200)
b_reset = frame.add_button("Reset", s_watch.reset, 200)
timer = simplegui.create_timer(100, s_watch.watch)

# register draw handler
frame.set_draw_handler(draw)

# start frame
frame.start()
